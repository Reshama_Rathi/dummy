//
//  ContentView.swift
//  dummy
//
//  Created by Reshama Rathi on 14/02/20.
//  Copyright © 2020 Reshama Rathi. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
